<?php

use ddn\sapp\PDFDoc;

// Include the DeaFonso library
require_once('vendor/autoload.php');


// Check if the form has been submitted
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    // Get the uploaded image file
    $pdfUpload = $_FILES['pdf']['tmp_name'];
    $image = $_FILES['image']['tmp_name'];
    $certificat = $_FILES['cert']['tmp_name'];
    
    if (!file_exists($pdfUpload)) {
        throw new InvalidArgumentException("Ouverture du PDF impossible");
    }
    
    if (!file_exists($image)) {
        throw new InvalidArgumentException("Ouverture de signature impossible");
    }
    
    if (!file_exists($certificat)) {
        throw new InvalidArgumentException("Ouverture du certificat impossible");
    }
    
    // Déplacez les fichiers téléchargés vers l'emplacement souhaité
    move_uploaded_file($pdfUpload, 'uploaded_pdf.pdf');
    move_uploaded_file($image, 'uploaded_signature.png');
    move_uploaded_file($certificat, 'uploaded_certificat.p12');
    
    
    // Créer un objet de la classe PDFDoc à partir d'une chaîne de caractères qui contient le contenu d'un fichier PDF
    $obj = PDFDoc::from_string(file_get_contents("uploaded_pdf.pdf"));
	
    if (false === $obj) {
        throw new InvalidArgumentException("Analyse du fichier impossible");
    }
    
    $imageSize = @getimagesize("uploaded_signature.png");
    
    if (false === $imageSize) {
        throw new InvalidArgumentException("impossible d'ouvrir l'image uploaded_signature.png");
    }
    
    $pageSize = $obj->get_page_size(0);
    
    if (false === $pageSize) {
        throw new InvalidArgumentException("impossible d'obtenir la taille de la page");
    }
    
    $pageSize = explode(" ", $pageSize[0]->val());
    
    // Calcule la position de l'image en fonction de sa taille et de la taille de la page ;
    // l'idée est de conserver les proportions et de centrer l'image dans la page avec une taille
    // de 1/3 de la taille de la page.
    $p_x = (int)("" . $pageSize[0]);
    $p_y = (int)("" . $pageSize[1]);
    $p_w = (int)("" . $pageSize[2]) - $p_x;
    $p_h = (int)("" . $pageSize[3]) - $p_y;
    
    //Largeur
    $i_w = $imageSize[0];
    //Hauteur
    $i_h = $imageSize[1];
    
    $ratio_x = $p_w / $i_w;
    $ratio_y = $p_h / $i_h;
    $ratio = min($ratio_x, $ratio_y);
    
    $i_w = ($i_w * $ratio) / 3;
    $i_h = ($i_h * $ratio) / 3;
    $p_x = $p_w / 3;
    $p_y = $p_h / 3;
    
    $obj->set_signature_appearance(0, [$p_x, $p_y, $p_x + $i_w, $p_y + $i_h], 'uploaded_signature.png');
    
    //Le mot de passe du certificat
    $password = '22478105';
    if (!$obj->set_signature_certificate('uploaded_certificat.p12', $password)) {
        throw new InvalidArgumentException("Certificat invalide");
    }
    
    $docSigned = $obj->to_pdf_file_s(true);
    if (!$docSigned) {
        throw new InvalidArgumentException("Impossible de signer le fichier PDF");
    }
	 echo $docSigned;
    
}