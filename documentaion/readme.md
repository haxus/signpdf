### Avant de commencer :
- Le certificat doit être valide (Je peux vous fournir un tuto qui vous montre comment créer un certificat ".p12"
  gratuitement sans l'acheter)
- La version du fichier PDF à signer doit être supérieure ou égale à 1.15

### Exemple en action :
- Pour l'ajout d'une signature il faut choisir un PDF (>=1.5), un certificat valide (Je vous ai fourni mon 
  certificat actuellement pour tester) et une signature (image png)
![img.png](img.png)
