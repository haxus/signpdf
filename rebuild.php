<?php

use ddn\sapp\PDFDoc;

// Include the DeaFonso library
require_once('vendor/autoload.php');


// Check if the form has been submitted
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    // Get the uploaded image file
    $pdfUpload = $_FILES['rebuild']['tmp_name'];
  
    // Déplacez le fichier téléchargé vers l'emplacement souhaité
    move_uploaded_file($pdfUpload, 'uploaded_pdf.pdf');
    
    // Créer un objet de la classe PDFDoc à partir d'une chaîne de caractères qui contient le contenu d'un fichier PDF
    $obj = PDFDoc::from_string(file_get_contents("uploaded_pdf.pdf"));
    
    if (false === $obj){
        fwrite(STDERR, "failed to parse file " . $argv[1]);
    }else{
       echo $obj->to_pdf_file_s(true);
    }
}
