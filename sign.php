<?php

use ddn\sapp\PDFDoc;

// Include the DeaFonso library
require_once('vendor/autoload.php');


// Check if the form has been submitted
if ('POST' === $_SERVER['REQUEST_METHOD']) {
	// Get the uploaded image file
	$pdfUpload = $_FILES['sign']['tmp_name'];
	$certificat = $_FILES['cert1']['tmp_name'];
	if (!file_exists($pdfUpload)) {
		throw new InvalidArgumentException("failed to open file");
	}
	
	
	// Déplacez le fichier téléchargé vers l'emplacement souhaité
	move_uploaded_file($pdfUpload, 'uploaded_pdf.pdf');
	move_uploaded_file($certificat, 'uploaded_certificat.p12');
	
	// Créer un objet de la classe PDFDoc à partir d'une chaîne de caractères qui contient le contenu d'un fichier PDF
	$obj = PDFDoc::from_string(file_get_contents("uploaded_pdf.pdf"));
	//Le mot de passe de mon certificat
	$password = '22478105';
	if (false === $obj) {
		throw new InvalidArgumentException("Analyse du fichier impossible");
	}
	elseif (!$obj->set_signature_certificate('uploaded_certificat.p12', $password)) {
		throw new InvalidArgumentException("Certificat invalide");
	}
	else {
		$docSigned = $obj->to_pdf_file_s();
		if (false === $docSigned) {
			throw new InvalidArgumentException("Impossible de signer le fichier PDF");
		}
		else {
			$docSigned = $obj->to_pdf_file_s();
			//echo $docSigned;
		}
		
	}
}